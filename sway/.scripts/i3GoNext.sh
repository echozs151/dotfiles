#!/usr/bin/env sh

wsNext=$(( $( swaymsg -t get_workspaces | jq '.[] | select(.focused).num' ) + $1))
swaymsg workspace $wsNext
