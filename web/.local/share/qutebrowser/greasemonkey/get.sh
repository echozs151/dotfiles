#!/usr/bin/env sh

SCRIPT_DIR=${XDG_DATA_HOME:-$HOME/.local/share}/qutebrowser/greasemonkey

cd $SCRIPT_DIR

curl -O "https://www.4chan-x.net/builds/4chan-X.user.js"

curl -O "https://raw.githubusercontent.com/KevinParnell/OneeChan/master/builds/OneeChan.user.js"

curl -O 'https://raw.githubusercontent.com/rcc11/4chan-sounds-player/master/dist/4chan-sounds-player.user.js'

curl -O "https://gist.githubusercontent.com/olmokramer/fe23c8cdd29a8b567343aa575e5cdeaa/raw/81468af5b21e95f784e75954bb78930802b20bce/per-domain-stylesheets.user.js"

curl -O "https://greasyfork.org/scripts/386210-mangadex-filter/code/MangaDex%20Filter.user.js"
