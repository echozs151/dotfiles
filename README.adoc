= Dotfiles
include::header.adoc[]

[source, config]
----
Distro:                Void Linux
WM:                    BSPWM
DM:                    Xinit
Shell:                 Fish
Terminal:              Kitty
Panel:                 Polybar (+ xfce-panel for global menu)
Compositor:            Picom (branch with round corners and kawase blur)
Application Launcher:  Rofi
File Manager:          Vifm, Fzf, dolphin
Text Editor:           Neovim
Image viewer:          Imv
Video player:          Mpv
----

== Setup

The dotfiles are maintained with `stow`.
The custom script `setup.sh` read the list of packages present in `./packages.yaml` and automate the installation of necessary packages and services to be enabled.

WARNING: Backup your dotfiles before running the script.

This basic setup will get you BSPWM up and running with Kitty terminal and Neovim configuration.
[source, shell]
----
$ ./setup.sh terminal
$ ./setup.sh bspwm
$ ./setup.sh menu
$ ./setup.sh music
----

== Music

image:./.pictures/neofetch-music-cover.png[width=100%]

Album cover is generated with link:./music/.config/ncmpcpp/art.sh[`~/.config/ncmpcpp/art.sh`], the cover pictured is searched and `ffmpeg` is used to generate the thumbnail with round borders and cropped to 300px.
The cover is displayed using Kitty icat with link:./music/.config/ncmpcpp/kitty.sh[`~/.config/ncmpcpp/kitty.sh`].

The alias `music` used to open the music player make just use of background processes to have the cover preview and ncmpcpp on the same window.
[source,shell]
----
abbr music 'mpc -q play; ~/.config/ncmpcpp/art.sh; sleep 1; ~/.config/ncmpcpp/kitty.sh &; ncmpcpp'
----

== Browse files

image:./.pictures/important-work-manga.png[width=100%]

=== Vifm

Vifm has image preview activated using the custom script link:./terminal/.config/vifm/scripts/preview[`~/.config/vifm/scripts/preview`], can be previewed pictures, video files, music spectrograms, epub and pdfs.

* Set wallpaper and update color scheme with kbd:[w]
* Extract archives with kbd:[x]

=== Fzf

The terminal has integrated fzf support. A preview script is available at link:./terminal/.config/kitty/preview[`~/.config/kitty/preview`]

* Find files with kbd:[Ctrl + t]
* Command history with kbd:[Ctrl + r]

=== Imv

Imv image viewer has been tuned to work with link:https://github.com/Liupold/pidswallow[window swallowing] and to be a very good manga reader.

* Running the `manga` command defined at link:./terminal/.config/fish/functions/manga.fish[`~/.config/fish/functions/manga.fish`] will open recursively all files in the folder sorted by name. It will also open the last visited page for it is tracking reading progress.
* The window title is updated with `[<current-page>/<total-page>]: <folder-name>` status.
* Next page start at the top (initial_pan = 0 0)
* Change scaling mode with kbd:[s]
* Tag a picture with kbd:[m], tags are available under `~/tags`

NOTE: Files stored in tags can be displayed with kbd:[Super + Shift + t ]

==== How it works

Imv is run with window properties changed using this black magic spell.
[source,shell]
----
alias imv 'imv -c "exec _wid=\$(xdo id);xprop -f WM_CLIENT_MACHINE 8s -set WM_CLIENT_MACHINE \$(hostname) -id \$_wid;xprop -f _NET_WM_PID 32c -set _NET_WM_PID \$imv_pid -id \$_wid;pidswallow -gt \$_wid"'
----

Reading progress is stored under `~/.cache/imv/` in the base64 hash of the directory path.

The imv config link:./terminal/.config/imv/config[`~/.config/imv/config`] has the following line that update the window title and store reading progress.

[source,ini]
----
title_text = $(xdotool getactivewindow set_window --name "[ $imv_current_index / $imv_file_count ] : $(echo "${PWD##*/}")"; echo "$imv_current_index" > ~/.cache/imv/$(echo $PWD | base64 -w0) )
----

== Launch application

image:./.pictures/rofi.png[width=100%]

Rofi is the application launcher and is called with kbd:[Super + Space].
Background color is updated with the theme using link:https://github.com/dylanaraps/pywal[`pywal`] templates. 

Install rofi with:
[source, shell]
----
$ ./setup.sh menu
----

== Read manga

image:./.pictures/rofi-manga.png[width=100%]

Have rofi manage your manga with kbd:[Super + Shift + Space ]. The selected volume will be open with the `manga` utility explained before.

To generate new entries execute this command defined at link:./terminal/.config/fish/functions/add_manga.fish[`~/.config/fish/functions/add_manga.fish`] with the full path to the manga.
[source, shell]
----
$ add_manga /path/to/manga
----

This utility works best with official releases bundled in `.cbz` files.
Use this command to extract images from `.cbz` releases.
[source, shell]
----
find -type f -iname '*.cbz' | xargs -I% atool -x %
----

NOTE: The cover images are automatically generated with the first image of each volume and available in `~/.config/manga`. 

NOTE: Each entry is in fact a `.desktop` file found in `~/.local/share/applications`.

NOTE: In this screenshot the entries were generated with `add_manga ~/Books/Sleepy\ Princess\ in\ the\ Demon\ Castle\ \(Digital\)\ \(danke-Empire\)/`.


== Qt theme and global menu

image:./.pictures/files-find-a-bird.png[width=100%]

Qt applications use Kvantum with link:https://store.kde.org/p/1365482/[Inverse-dark theme].
Global menu is set with `xfce4-panel` that was just moved on top of polybar.

Install qt applications with:
[source, shell]
----
$ ./setup.sh qtapps
----

